%MATLAB 2020b
%name: air_quailty
%author: jakugre
%date: 2020-11-22
%version: v1.5
%script file has to be in the same directory where the directory with csv
%files is

%Manually state acceptable levels for each substance
accept_levels = [350, 200, 0, 0, 0, 120, 0, 10000, 0];
accept_levels_for_8h = [0, 0, 0, 0, 0, 120, 0, 10000, 0];
accept_levels_for_24h = [0, 0, 0, 0, 0, 0, 0, 0, 50];

%Create variables that will hold numbers of hours and days
hours_in_total = "";
days_in_total = "";

%Get files from directory
files = dir('air_quality_data_Zabrze');

%Create and open file that will hold results and comments
result_file = fopen('PMresult.dat','w');

%Create formats of data for single accidents and for 8 and 24 hours
format = 'Warning! Value was exceeded!    Date: %s  Time: %s     Name: %s     Limit: %.1f [µg/m3]     Value %.1f [µg/m3] \n';
format_8h = 'Warning! Average value from last 8h exceeded!     Date: %s     Time: %s     Name: %s     Limit: %.1f [µg/m3]     Average value from last 24h: %.1f [µg/m3] \n';
format_24h = 'Warning! Average value from last 24h exceeded!     Date: %s     Time: %s     Name: %s     Limit: %.1f [µg/m3]     Average value from last 24h: %.1f [µg/m3] \n';

%Create tables for values of date, levels and time and headers from each
%file
date_values = [];
levels_values = [];
time_values = [];
headers = [];

%Number of files in directory - later on we should substract 2 because
%there are additional files except csv files
len = length(files);

%Do the same loop for each file in directory
for k = [3:len]
    %Get a file name 
    name  = files(k).name;
    
    %Merge name with directory name so that the script doesn't have to be
    %in the same directory as csv files
    name = strcat('air_quality_data_Zabrze\',name);
    
    %Read file line by line and transform raw data into data that could be read easily
    table = readlines(name);
    table = table(1:28,:);
    table = split(table,';');
    table = replace(table,',','.');
    table = replace(table,'"','');
    
    %Get the headers from the table and delete characters we don't need
    headers = table(1,2:10);
    headers = replace(headers,'[µg/m3]','');
    headers = replace(headers,'poz. dop.:','');
    headers = replace(headers,'1000','');
    headers = replace(headers,'200','');
    headers = replace(headers,'100','');
    
    
    
    
    %Extract time values from each file and add it to time list
    time = table(2:25,1);
    time_values = cat(1,time_values,time);
    
    %Extract values from table, replace blanks with zeros and then add it
    %to levels list so values can be scanned it 24-hour interval from the
    %very beginning
    values = table(2:25,2:10);
    values = replace(values,'-','0');
    values = double(values);
    levels_values = cat(1,levels_values,values);
    
    
    %Extract each date from file's name and add it 24 times each to date
    %list so later on we can get full date (day + hour)
    date = replace(name,'air_quality_data_Zabrze\dane-pomiarowe_','');
    date = replace(date,'.csv','');
    for i = [1:24]
        date_values = cat(1,date_values,date);
    end
   
    
end

%Execute loop for each row in table of values from each file and for each
%column
for i = [1:length(accept_levels)]
    if accept_levels(i) ~= 0
        for j = [1:length(time_values)]
            
            %Compare level values with acceptable levels and if the level
            %value exceeded the acceptable level - write it to the result
            %file with date, hour, name of substance, and add time to list
            %of hours and days to later comparison
            if levels_values(j,i) > accept_levels(i)
                fprintf(result_file,format,[date_values(j,:),time_values(j),headers(i),levels_values(j,i)])
                hours_in_total = hours_in_total +' '+ time_values(j) +' '+ date_values(j,:) + ',';
                days_in_total = days_in_total +' '+ date_values(j,:) + ',';
            end
        end
    end
end

%Go through list of values by 24-hour interval
for l = [1:length(accept_levels_for_24h)]
    if accept_levels_for_24h(l) ~= 0
        for m = [1:length(time_values)-23]
            
            %Calculate average value for each 24 values
            temp = sum(levels_values(m:m+23,l))/24;
            
            %If the average value exceeds acceptable level for 24-hour
            %interval, write it to file and with date, time, name of
            %substance and add time to list of hours and days
            if temp > accept_levels_for_24h(l)
                fprintf(result_file,format_24h,[date_values(m+23,:),time_values(m+23),headers(l),accept_levels_for_24h(l),temp]);
                hours_in_total = hours_in_total +' '+ time_values(m) +' '+ date_values(m,:) + ',';
                days_in_total = days_in_total +' '+ date_values(m,:) + ',';
                
            end
        end
    end
end


for x = [1:length(accept_levels_for_8h)]
    if accept_levels_for_8h(x) ~= 0
        for z = [1:length(time_values)-7]
            
            %Calculate average value for each 8 values
            temp = sum(levels_values(z:z+7,x))/8;
            
            %If the average value exceeds acceptable level for 8-hour
            %interval, write it to file and with date, time, name of
            %substance and add time to list of hours and days
            if temp > accept_levels_for_8h(x)
                fprintf(result_file,format_8h,[date_values(z+7,:),time_values(z+7),headers(x),accept_levels_for_8h(x),temp]);
                hours_in_total = hours_in_total +' '+ time_values(z) +' '+ date_values(z,:) + ',';
                days_in_total = days_in_total +' '+ date_values(z,:) + ',';
                
            end
        end
    end
end
%Number of days in general is equal to number of files
number_of_days = len-2;

%Make a list of days when any value was exceeded by splitting one string
days_in_total = split(days_in_total,',');

%Then delete duplicated days
days_in_total = unique(days_in_total);

%Make a list of specific hours (with dates) when values were exceeded
hours_in_total = split(hours_in_total,',');

%Delete duplicated hours
hours_in_total = unique(hours_in_total);

%Count how many times values were exceeded
number_of_hours = length(hours_in_total);
number_of_days_over_the_limit = length(days_in_total);


%Write results. In average values stated time is the time when certain
%interval ended.
fprintf(result_file,'\n \nThere were %d days in general, which equals %.f weeks \n',number_of_days, number_of_days/7);
fprintf(result_file,'There were %.1f hours per day on average when the limit was exceeded \n',number_of_hours/number_of_days);
fprintf(result_file,'On average, there were %.1f days per week when the limit was exceeded \n',number_of_days_over_the_limit/(number_of_days/7));
fprintf(result_file,'Some values were missing due to some unknown circumstances - that might have lowered or increased the average values');

%Close the file
fclose(result_file);

type PMresult.dat




    

